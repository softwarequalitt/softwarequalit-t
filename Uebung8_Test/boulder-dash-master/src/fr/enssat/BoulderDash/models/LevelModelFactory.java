package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.helpers.AudioLoadHelper;
import fr.enssat.BoulderDash.helpers.LevelLoadHelper;

public class LevelModelFactory {

	private static final int LEVELMODEL_SIZE_WIDTH_HEIGHT = 27;

	public static LevelModel createLevelModelModeGame(AudioLoadHelper audioLoadHelper,
			LevelLoadHelper levelLoadHelper, GameInformationModel gameInformationModel) {
		
		LevelModel levelModel = new LevelModel(audioLoadHelper, "game");

		levelModel.setGameRunning(true);
		
		levelModel.setGamePaused(false);

		levelModel.setLevelHelperData(levelLoadHelper);
		
		levelModel.setGameInformation(gameInformationModel);
		
		levelModel.setLevelLoadHelper(levelLoadHelper);
		
		levelModel.prepareAnimatorAndRockford();

		levelModel.makeCursorModel();

		levelModel.createLimits();
		
		return levelModel;
	}

	public static LevelModel createLevelModelModeEditor(AudioLoadHelper audioLoadHelper) {
		
		LevelModel levelModel = new LevelModel(audioLoadHelper, "editor");

		levelModel.setGameRunning(false);
		
		levelModel.setSizeHeight(LEVELMODEL_SIZE_WIDTH_HEIGHT);
		
		levelModel.setSizeWidth(LEVELMODEL_SIZE_WIDTH_HEIGHT);
		
		levelModel.setGroundGrid();

		levelModel.createLimits();
		
		return levelModel;
	}
}
