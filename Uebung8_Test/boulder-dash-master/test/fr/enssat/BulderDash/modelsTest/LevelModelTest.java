package fr.enssat.BulderDash.modelsTest;

import fr.enssat.BoulderDash.models.LevelModel;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import java.lang.reflect.Method;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import fr.enssat.BoulderDash.helpers.AudioLoadHelper;
import fr.enssat.BoulderDash.helpers.LevelLoadHelper;
import fr.enssat.BoulderDash.models.DisplayableElementModel;
import fr.enssat.BoulderDash.models.DoorModel;
import fr.enssat.BoulderDash.models.GameInformationModel;
import fr.enssat.BoulderDash.models.LevelModelFactory;

public class LevelModelTest {

	@Test
	public void spawnExitTest() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		AudioLoadHelper audioLoadHelper = Mockito.mock(AudioLoadHelper.class);
		
		LevelLoadHelper levelLoadHelper = Mockito.mock(LevelLoadHelper.class);
		
		GameInformationModel gameInformationModel = Mockito.mock(GameInformationModel.class);
		
		LevelModel lvlModel = LevelModelFactory.createLevelModelModeGame(audioLoadHelper, levelLoadHelper, gameInformationModel);
		
		Method method = lvlModel.getClass().getDeclaredMethod("spawnExit");
		
		method.setAccessible(true);
		
		lvlModel.setGroundGrid();
		
		// method.invoke();

		// TODO : Prüfe ob in dem grid des LevelModels ein DoorModel existiert
		
		DisplayableElementModel[][] groundGrid = lvlModel.getGroundLevelModel();
		
		Boolean doorModelExists = false;
		
		for(int i = 0; i < groundGrid.length ; i++ ) {
			
			for (int j = 0 ; i < groundGrid[i].length; j++) {
				
				if (groundGrid[i][j] instanceof DoorModel) {
					
					doorModelExists = true;
					
				}
				
			}

		}
		
		assertTrue(doorModelExists);
		
	}

}
