package data;

public class Dice {
	
	private int id;
	private int actualValue;
	
	public Dice(int id){
		this.id = id;
	}
	
	public Dice(){
		
	}
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dice other = (Dice) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getActualValue() {
		return actualValue;
	}
	public void setActualValue(int actualValue) {
		this.actualValue = actualValue;
	}
	
	
	
}
