package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import data.Dice;
import data.Player;
import utility.GameUtility;

public class Fenster extends JFrame{

	private static final String TITEL= "WUERFELSPIEL";
	private static final int FENSTER_BREITE = 400;
	private static final int FENSTER_HOEHE = 400;
	
	private static final int TEXTFIELD_BREITE = 40;
	private static final int TEXTFIELD_HOEHE = 20;
	
	private static final int CHECKBOX_BREITE = 20;
	private static final int CHECKBOX_HOEHE = 20;
	
	private static final int Y_COORDINATE_CHECKBOX = 200;
	private static final int Y_COORDINATE_TEXTFELD = 160;
	
	//private static fint int 
	
	private JButton wuerfeln;
	private JCheckBox wuerfelEins;
	private JCheckBox wuerfelZwei;
	private JCheckBox wuerfelDrei;
	private JCheckBox wuerfelVier;
	private JCheckBox wuerfelFuenf;
	private JCheckBox wuerfelSechs;
	
	private JTextField wuerfelFeldEins;
	private JTextField wuerfelFeldZwei;
	private JTextField wuerfelFeldDrei;
	private JTextField wuerfelFeldVier;
	private JTextField wuerfelFeldFuenf;
	private JTextField wuerfelFeldSechs;
	
	private JTextField score;
	private JTextField playerOneScore;
	private JTextField playerTwoScore;
	private JLabel namePlayerOne;
	private JLabel namePlayerTwo;
	private JLabel actualPlayer;
	
	private JButton nextPlayer;
	
	private List<JCheckBox> checkBoxList;
	private List<JTextField> textFieldList;
	
	private Player player;
	
	public Fenster(){
		this.setSize(FENSTER_BREITE, FENSTER_HOEHE);
		this.setTitle(TITEL);
		this.setLayout(null);
	
		namePlayerOne = new JLabel();
		namePlayerOne.setBounds(120,30, 60, 30);
		
		namePlayerTwo = new JLabel();
		namePlayerTwo.setBounds(120,70, 60, 30);
		
		actualPlayer = new JLabel();
		actualPlayer.setBounds(250, 100, 100, 30);
		
		playerOneScore = new JTextField();
		playerOneScore.setBounds(170,30, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		playerOneScore.setText("0");
		
		playerTwoScore = new JTextField();
		playerTwoScore.setBounds(170,70, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		playerTwoScore.setText("0");
	
		score = new JTextField();
		score.setBounds(50,30, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		score.setText("0");
		
		wuerfelFeldEins = new JTextField();
		wuerfelFeldEins.setBounds(50,Y_COORDINATE_TEXTFELD, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		wuerfelFeldZwei = new JTextField();
		wuerfelFeldZwei.setBounds(100,Y_COORDINATE_TEXTFELD, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		wuerfelFeldDrei = new JTextField();
		wuerfelFeldDrei.setBounds(150,Y_COORDINATE_TEXTFELD, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		wuerfelFeldVier = new JTextField();
		wuerfelFeldVier.setBounds(200,Y_COORDINATE_TEXTFELD, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		wuerfelFeldFuenf = new JTextField();
		wuerfelFeldFuenf.setBounds(250,Y_COORDINATE_TEXTFELD, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		wuerfelFeldSechs = new JTextField();
		wuerfelFeldSechs.setBounds(300,Y_COORDINATE_TEXTFELD, TEXTFIELD_BREITE, TEXTFIELD_HOEHE);
		
		wuerfelEins = new JCheckBox();
		wuerfelEins.setBounds(50, Y_COORDINATE_CHECKBOX, CHECKBOX_BREITE, CHECKBOX_HOEHE);
		wuerfelZwei = new JCheckBox();
		wuerfelZwei.setBounds(100, Y_COORDINATE_CHECKBOX, CHECKBOX_BREITE, CHECKBOX_HOEHE);
		wuerfelDrei = new JCheckBox();
		wuerfelDrei.setBounds(150, Y_COORDINATE_CHECKBOX, CHECKBOX_BREITE, CHECKBOX_HOEHE);
		wuerfelVier = new JCheckBox();
		wuerfelVier.setBounds(200, Y_COORDINATE_CHECKBOX, CHECKBOX_BREITE, CHECKBOX_HOEHE);
		wuerfelFuenf = new JCheckBox();
		wuerfelFuenf.setBounds(250, Y_COORDINATE_CHECKBOX, CHECKBOX_BREITE, CHECKBOX_HOEHE);
		wuerfelSechs = new JCheckBox();
		wuerfelSechs.setBounds(300, Y_COORDINATE_CHECKBOX, CHECKBOX_BREITE, CHECKBOX_HOEHE);
		
		clickListener(wuerfelEins,0);
		clickListener(wuerfelZwei,1);
		clickListener(wuerfelDrei,2);
		clickListener(wuerfelVier,3);
		clickListener(wuerfelFuenf,4);
		clickListener(wuerfelSechs,5);
		
		checkBoxList = new LinkedList<>();
		checkBoxList.add(wuerfelEins);
		checkBoxList.add(wuerfelZwei);
		checkBoxList.add(wuerfelDrei);
		checkBoxList.add(wuerfelVier);
		checkBoxList.add(wuerfelFuenf);
		checkBoxList.add(wuerfelSechs);
		
		textFieldList = new LinkedList<>();
		textFieldList.add(wuerfelFeldEins);
		textFieldList.add(wuerfelFeldZwei);
		textFieldList.add(wuerfelFeldDrei);
		textFieldList.add(wuerfelFeldVier);
		textFieldList.add(wuerfelFeldFuenf);
		textFieldList.add(wuerfelFeldSechs);
		
		nextPlayer = new JButton("next Player");
		nextPlayer.setBounds(130, 100, 100, 30);
		actualPlayer.setText(GameUtility.getInstance().actualPlayer().getName());
		nextPlayer.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				int tempScore = Integer.valueOf(score.getText());
				GameUtility.getInstance().actualPlayer().addScore(tempScore);
				
				if(GameUtility.getInstance().actualPlayer().getName().equals("Karl")){
					playerOneScore.setText(String.valueOf(GameUtility.getInstance().actualPlayer().getScore()));
				} else {
					playerTwoScore.setText(String.valueOf(GameUtility.getInstance().actualPlayer().getScore()));
				}
			
				GameUtility.getInstance().nextPlayer();
				player = GameUtility.getInstance().actualPlayer();
				actualPlayer.setText(player.getName());
				
				for(JCheckBox chBox: checkBoxList){
					chBox.setSelected(false);
					chBox.setEnabled(true);
				}
				GameUtility.getInstance().setFixDiceList(new LinkedList<Dice>());
				for(JTextField field:textFieldList){
					field.setText("");
				}
				GameUtility.getInstance().setPlayerScore(0);
				score.setText("0");
				
				
			}
			
		});
		
		wuerfeln = new JButton();
		wuerfeln.setBounds(0, 100, 60, 30);
		wuerfeln.setText("roll");
		wuerfeln.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<Dice> diceTemp = GameUtility.getInstance().getDiceList();
				List<Dice> values = new LinkedList<>(); 
				for(Dice dice:diceTemp){
					if(!GameUtility.getInstance().getFixDiceList().contains(dice)){
						GameUtility.getInstance().rollDice(dice);
						values.add(dice);
					}
				}
				
				
				for(int i=0; i < 6; i++){
					textFieldList.get(i).setText(String.valueOf(diceTemp.get(i).getActualValue()));
					
				}
				boolean next = false;
				
				for(Dice dice : values){
					next = dice.getActualValue() == 1 || dice.getActualValue() == 5;
					System.out.println("Test values of is 1 or 5: " + dice.getActualValue());
					if(next){
						break;
					}
				}
				System.out.println("#########################################################");
				
				if(!next){
					score.setText("0");
					nextPlayer.doClick();
				}
				for(JCheckBox chBox: checkBoxList){
					if(chBox.isSelected()){
						chBox.setEnabled(false);
					}
				}
				
			}
			
		});

		this.add(wuerfeln);
		this.add(wuerfelEins);
		this.add(wuerfelZwei);
		this.add(wuerfelDrei);
		this.add(wuerfelVier);
		this.add(wuerfelFuenf);
		this.add(wuerfelSechs);
		this.add(wuerfelFeldEins);
		this.add(wuerfelFeldZwei);
		this.add(wuerfelFeldDrei);
		this.add(wuerfelFeldVier);
		this.add(wuerfelFeldFuenf);
		this.add(wuerfelFeldSechs);
		this.add(score);
		this.add(namePlayerOne);
		this.add(namePlayerTwo);
		this.add(playerOneScore);
		this.add(playerTwoScore);
		this.add(nextPlayer);
		this.add(actualPlayer);
		
		this.setVisible(true);
	}
	
	public void clickListener(JCheckBox checkBox, int position){
		checkBox.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkBox.isSelected()){
					GameUtility.getInstance().setPlayerScore(GameUtility.getInstance().changeDiceStatus(position));
					score.setText(String.valueOf(GameUtility.getInstance().getPlayerScore()));
				} else {
					Dice temp = GameUtility.getInstance().getDiceList().get(position);
					GameUtility.getInstance().removeInFixDiceList(temp);	
					score.setText(String.valueOf(GameUtility.getInstance().getPlayerScore()));
				}
			}
			
		});
	}

	public List<JCheckBox> getCheckBoxList() {
		return checkBoxList;
	}

	public void setCheckBoxList(List<JCheckBox> checkBoxList) {
		this.checkBoxList = checkBoxList;
	}

	public List<JTextField> getTextFieldList() {
		return textFieldList;
	}

	public void setTextFieldList(List<JTextField> textFieldList) {
		this.textFieldList = textFieldList;
	}

	public JTextField getScore() {
		return score;
	}

	public void setScore(JTextField score) {
		this.score = score;
	}

	public void setNamePlayerOne(String name){
		namePlayerOne.setText(name);
	}
	
	public void setNamePlayerTwo(String name){
		namePlayerTwo.setText(name);
	}
	
	

}
