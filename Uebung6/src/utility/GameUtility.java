package utility;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import data.Dice;
import data.Player;

public class GameUtility {
	
	private int playerScore = 0;
	
	private Queue<Player> playerList;
	private List<Dice> diceList = new LinkedList<>();
	private List<Dice> fixDiceList = new LinkedList<>();
	
	private static GameUtility singleton = new GameUtility();

	private GameUtility() {

	}
	
	public void nextPlayer(){
		Player temp = this.playerList.poll();
		this.playerList.add(temp);
	}

	public Player actualPlayer(){
		Player player = this.playerList.peek();
		return player;
	}

	public void newRound(){
		fixDiceList = new LinkedList<>();
	}
	public static GameUtility getInstance() {
		return singleton;
	}

	public void rollDice(Dice dice) {
		int value = (int) ((Math.random()*6)+1);
		dice.setActualValue(value);
	}

	public int changeDiceStatus(int position){
		
		int erg = 0;
		Dice dice = diceList.get(position);
		fixDiceList.add(dice);
		erg = score(fixDiceList);
		return erg;
		
	}
	
	public int score(List<Dice> diceList) {
		int scoreValue = 0;
		int counterOne = 0;
		int counterFive = 0;

		for (Dice dice : diceList) {

			if (dice.getActualValue() == 1) {
				counterOne++;
			}

			if (dice.getActualValue() == 5) {
				counterFive++;
			}
		}
		
		scoreValue += scoreOne(counterOne);
		scoreValue += scoreFive(counterFive);

		return scoreValue;
	}
	
	private int scoreOne(int value){
		switch(value){
		case 1: return 100;
		case 2: return 200;
		case 3: return 1000;
		case 4: return 1100;
		case 5: return 1200;
		case 6: return 2000;
		default: return 0;
		}
	}
	
	private int scoreFive(int value){
		switch(value){
		case 1: return 50;
		case 2: return 100;
		case 3: return 500;
		case 4: return 550;
		case 5: return 600;
		case 6: return 1000;
		default: return 0;
		}
	}
	
	public void removeInFixDiceList(Dice dice){
		
		if(fixDiceList.contains(dice)){
			fixDiceList.remove(dice);
			playerScore = score(fixDiceList);
		}
	}

	public List<Dice> getDiceList() {
		return diceList;
	}

	public void setDiceList(List<Dice> diceList) {
		this.diceList = diceList;
	}

	public List<Dice> getFixDiceList() {
		return fixDiceList;
	}

	public void setFixDiceList(List<Dice> fixDiceList) {
		this.fixDiceList = fixDiceList;
	}
	
	public int getPlayerScore() {
		return playerScore;
	}

	public void setPlayerScore(int playerScore) {
		this.playerScore = playerScore;
	}

	public Queue<Player> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(Queue<Player> playerList) {
		this.playerList = playerList;
	}
	
	
	

}
