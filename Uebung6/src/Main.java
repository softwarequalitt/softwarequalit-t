import gui.Fenster;
import utility.GameUtility;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import data.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Queue<Player> playerList = new LinkedList<>();
		Player karl = new Player("Karl");
		Player lenny = new Player("Lenny");
		
		playerList.add(karl);
		playerList.add(lenny);
		
		GameUtility.getInstance().setPlayerList(playerList);
		
		Fenster fenster = new Fenster();
		fenster.setNamePlayerOne(karl.getName());
		fenster.setNamePlayerTwo(lenny.getName());
		
		List<Dice> diceList = new LinkedList<>();
		 
		for(int i = 0; i < 6; i++){
			diceList.add(new Dice(i));
		}
		 
		for(Dice dice:diceList){
			GameUtility.getInstance().rollDice(dice);
		}
		 
		GameUtility.getInstance().setDiceList(diceList);
		
		List<JCheckBox> tempList = fenster.getCheckBoxList();
		List<JTextField>  textList = fenster.getTextFieldList();
		 
		 
		
		
		

	}

}
