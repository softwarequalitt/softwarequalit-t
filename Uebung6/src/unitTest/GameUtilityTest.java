package unitTest;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

import data.Dice;
import utility.GameUtility;
import data.Player;

public class GameUtilityTest {

	private List<Dice> diceList;
	private List<Dice> fixDiceList;
	private Queue<Player> playerList;
	private Player player;
	private Player player2;
	private int playerScore;
	private GameUtility game = GameUtility.getInstance();
	private static final int TEMPSCORE = 50;
	
	@Before
	public void prepare(){
		diceList = new LinkedList<>();
		fixDiceList = new LinkedList<>();
		playerList = new LinkedList<>();
		player = new Player("Hans");
		player2 = new Player("Karl");
		playerScore = 0;
	
	}
	
	@Test
	public void gameUtilityGetInstanceTest() {
		assertNotNull(GameUtility.getInstance());
	}
	
	@Test
	public void gameUtilitySetPlayerListTest() {
		playerList.add(player);
		playerList.add(player2);
		game.setPlayerList(playerList);
		int size = game.getPlayerList().size();
		int sizePlayerListOrginal = playerList.size();
		assertEquals(size,sizePlayerListOrginal);
	}
	
	@Test
	public void gameUtilityNextPlayerAndActualPlayerTest() {
		playerList.add(player);
		playerList.add(player2);
		game.setPlayerList(playerList);
		
		Player first = game.actualPlayer();
		assertTrue(first.equals(player));
		game.nextPlayer();
		Player second = game.actualPlayer();
		assertTrue(second.equals(player2));
		
	}
	
	@Test
	public void gameUtilitySetAndGetPlayerScoreTest() {
		game.setPlayerScore(TEMPSCORE);
		int temp = game.getPlayerScore();
		assertEquals(TEMPSCORE, temp);
	}
	
	@Test
	public void gameUtilitySetDiceListTest(){
		 
		for(int i = 0; i < 6; i++){
			diceList.add(new Dice(i));
		}
		 
		for(Dice dice:diceList){
			game.rollDice(dice);
		}
		 
		game.setDiceList(diceList);
		
		int utilitySize = game.getDiceList().size();
		int normalSize = diceList.size();
		
		assertEquals(utilitySize,normalSize);
	}
	
	@Test
	public void gameUtilitySetFixDiceListTest(){
		 
		for(int i = 0; i < 6; i++){
			fixDiceList.add(new Dice(i));
		}
		 
		game.setFixDiceList(fixDiceList);
		
		int utilitySize = game.getFixDiceList().size();
		int normalSize = fixDiceList.size();
		
		assertEquals(utilitySize,normalSize);
	}
	
	@Test
	public void gameUtilitynewRoundTest(){
		 
		for(int i = 0; i < 6; i++){
			fixDiceList.add(new Dice(i));
		}
		 
		game.setFixDiceList(fixDiceList);
		game.newRound();

		assertTrue(game.getFixDiceList().isEmpty());
	}
	
	@Test
	public void gameUtilityScoreOneTest(){

		assertEquals(100,100);

	}
	
	@Test
	public void gameUtilitychangeDiceStatusScoreOneTest(){
		int score;
		Dice dice;
		
		dice = new Dice();
		dice.setActualValue(1);
		diceList.add(dice);
		game.setDiceList(diceList);
		score = game.changeDiceStatus(0);
		assertEquals(100,score);
		
		dice = new Dice();
		dice.setActualValue(1);
		diceList.add(dice);
		score = game.changeDiceStatus(1);
		assertEquals(200,score);
		
		dice = new Dice();
		dice.setActualValue(1);
		diceList.add(dice);
		score = game.changeDiceStatus(2);
		assertEquals(1000,score);
		
		dice = new Dice();
		dice.setActualValue(1);
		diceList.add(dice);
		score = game.changeDiceStatus(3);
		assertEquals(1100,score);
		
		dice = new Dice();
		dice.setActualValue(1);
		diceList.add(dice);
		score = game.changeDiceStatus(4);
		assertEquals(1200,score);
		
		dice = new Dice();
		dice.setActualValue(1);
		diceList.add(dice);
		score = game.changeDiceStatus(5);
		assertEquals(2000,score);
		
		game.setFixDiceList(new LinkedList<>());
		dice = new Dice();
		dice.setActualValue(2);
		diceList = new LinkedList<>();
		diceList.add(dice);
		game.setDiceList(diceList);
		score = game.changeDiceStatus(0);
		assertEquals(0,score);
		

	}
	
	@Test
	public void gameUtilitychangeDiceStatusTest(){

		int score;
		Dice dice;
		
		dice = new Dice();
		dice.setActualValue(5);
		diceList.add(dice);
		game.setDiceList(diceList);
		score = game.changeDiceStatus(0);
		assertEquals(50,score);
		
		dice = new Dice();
		dice.setActualValue(5);
		diceList.add(dice);
		score = game.changeDiceStatus(1);
		assertEquals(100,score);
		
		dice = new Dice();
		dice.setActualValue(5);
		diceList.add(dice);
		score = game.changeDiceStatus(2);
		assertEquals(500,score);
		
		dice = new Dice();
		dice.setActualValue(5);
		diceList.add(dice);
		score = game.changeDiceStatus(3);
		assertEquals(550,score);
		
		dice = new Dice();
		dice.setActualValue(5);
		diceList.add(dice);
		score = game.changeDiceStatus(4);
		assertEquals(600,score);
		
		dice = new Dice();
		dice.setActualValue(5);
		diceList.add(dice);
		score = game.changeDiceStatus(5);
		assertEquals(1000,score);
		
		game.setFixDiceList(new LinkedList<>());
		dice = new Dice();
		dice.setActualValue(2);
		diceList = new LinkedList<>();
		diceList.add(dice);
		game.setDiceList(diceList);
		score = game.changeDiceStatus(0);
		assertEquals(0,score);

	}
	
	@Test
	public void gameUtilityRemoveInFixListTest(){
		Dice dice;
		game.setFixDiceList(new LinkedList<>());
		dice = new Dice();
		dice.setActualValue(2);
		fixDiceList.add(dice);
		game.setFixDiceList(fixDiceList);
		assertFalse(game.getFixDiceList().isEmpty());
		game.removeInFixDiceList(dice);
		
		assertTrue(game.getFixDiceList().isEmpty());
		game.removeInFixDiceList(dice);
		
	}
	
	
	
	
	

}
