package unitTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import data.Player;

public class PlayerTest {

	private Player player;
	private Player player2;
	private Player playerNull;
	private Player playerSame;
	
	@Before
	public void prepare() {
		player = new Player("Hans");
		playerSame = new Player("Hans");
		player2 = new Player("Kurt");
		playerNull = new Player(null);
	}
	
	@Test 
	public void playerConstructorTest(){
		assertNotNull(player);
	}
	
	@Test 
	public void playerSetScoreAndGetScoreTest(){
		player.setScore(5);
		assertEquals(player.getScore(),5);
	}
	
	@Test 
	public void playerSetNameandGetNameTest(){
		player.setName("Franz");
		assertEquals(player.getName(),"Franz");
	}
	
	@Test 
	public void playerAddScoreTest(){
		player.addScore(5);
		assertEquals(player.getScore(),5);
	}
	
	@Test 
	public void playerHashCodeTest(){
		int temp = 31+(player.getName().hashCode());
		assertEquals(player.hashCode(),temp);
	}
	
	@Test 
	public void playerHashCodeNameNullTest(){
		player.setName(null);
		assertEquals(player.hashCode(),31);
	}
	
	@Test 
	public void playerEqualsToHisSelfTest(){
		assertTrue(player.equals(player));
	}
	
	@Test 
	public void playerNotEqualsToNullTest(){
		assertFalse(player.equals(null));
	}
	
	@Test 
	public void playerEqualsPlayerWhitSameNameTest(){
		assertTrue(player.equals(playerSame));
	}
	
	@Test 
	public void playerNotToEqualsPlayerWhitAnotherNameTest(){
		assertFalse(player.equals(player2));
	}
	
	@Test 
	public void playerNotToEqualsPlayerWhitNullAsNameTest(){
		assertFalse(player.equals(playerNull));
	}
	
	@Test 
	public void playerNameNullIsNotEqualToPlayerWhitNameTest(){
		assertFalse(playerNull.equals(player));
	}
	
	@Test 
	public void playerNameNullIsEqualToPlayerWhitNullNameTest(){
		player.setName(null);
		assertTrue(playerNull.equals(player));
	}
	
}
