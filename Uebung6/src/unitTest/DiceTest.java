package unitTest;

import static org.junit.Assert.*;

import org.junit.Test;

import data.Dice;

public class DiceTest {


	@Test
	public void DiceSetActualValueWhitgetActualValueTest() {
		Dice dice = new Dice();
		dice.setActualValue(8);
		assertEquals(8,dice.getActualValue());
	}
	
	
	@Test
	public void createDiceWhitoutParameterTest() {
		Dice dice = new Dice();
		assertNotNull(dice);
	}
	
	@Test
	public void createDiceWhitParameterTest() {
		Dice dice = new Dice(1);
		assertNotNull(dice);
	}
	
	@Test
	public void diceIsEqualToHisSelfWhitoutParamterTest(){
		Dice dice = new Dice();
		assertTrue(dice.equals(dice));
	}
	
	@Test
	public void diceIsEqualToHisSelfWhitParamterTest(){
		Dice dice = new Dice(1);
		assertTrue(dice.equals(dice));
	}
	
	@Test
	public void diceIsEqualToAnotherDiceWhitSameIdTest(){
		Dice dice = new Dice(1);
		Dice diceTwo = new Dice(1);
		assertTrue(dice.equals(diceTwo));

	}
	
	@Test
	public void diceNotEqualToDiceWhitAnotherIdTest(){
		Dice dice = new Dice(1);
		Dice diceTwo = new Dice(2);
		assertFalse(dice.equals(diceTwo));

	}
	
	@Test
	public void diceWhitParameterIsNotEqualToNullTest(){
		Dice dice = new Dice(1);
		assertFalse(dice.equals(null));

	}
	
	@Test
	public void diceWhitoutParameterIsNotEqualToNullTest(){
		Dice dice = new Dice();
		assertFalse(dice.equals(null));

	}
	
	@Test
	public void diceHasNotTheSameClassAsTheSecondDiceTest(){
		Dice dice = new Dice(1);
		Dice diceTwo = new Dice(2);
		assertFalse(dice.getClass()!=(diceTwo.getClass()));

	}
	
	@Test
	public void diceHasCodeTest(){
		Dice dice = new Dice(1);
		
		assertEquals(dice.hashCode(),32);

	}


}
