package seleniumTests;

import static org.junit.Assert.*;

import java.awt.event.ItemEvent;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BahnTest {
	
	WebDriver driver;
	
	@Before
	public void initTest() {
		
		System.setProperty("webdriver.chrome.driver", "/Users/sebastian/Development/SeleniumAddOns/chromeDriver");
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		driver.get("http://www.bahn.de");
	}

	@Test
	public void webSiteIsBahndeTest() {
	
		WebElement bahnLogoImage = driver.findElement(By.xpath("//img[contains(@src,'/common/view/static/v8/img/db_em_rgb_100px.svg')]"));
		
		assertTrue(bahnLogoImage.isDisplayed());
		
		
	}
	
	@Test
	public void mietwagenUndUmweltTest() {
		
		WebElement mwAgentBtn = driver.findElement(By.id("qf-node-mwagent-umwelt"));
		
		mwAgentBtn.click();
		
		WebElement mwMietenBtn = driver.findElement(By.name("qf.mietwagen.button.suchen"));
		
		assertTrue(mwMietenBtn.isDisplayed());
		
		WebElement mietDatumTextField = driver.findElement(By.id("qf-mietwagen-rental-date"));
		
		mietDatumTextField.clear();
		
		mietDatumTextField.sendKeys("24.12.2016");
		
		mietDatumTextField.sendKeys(org.openqa.selenium.Keys.TAB);
		
		assertTrue(mietDatumTextField.getAttribute("value").contains("Sa,"));
		
		WebElement returnDatumTextField = driver.findElement(By.id("qf-mietwagen-return-date"));
		
		assertTrue(returnDatumTextField.getAttribute("value").contains("25.12.16"));
		
		WebElement anmietStationTextField = driver.findElement(By.id("qf-mietwagen-rent"));
		
		anmietStationTextField.sendKeys("Marburg");
		
		anmietStationTextField.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 5);

		driver.getPageSource();
//		
//		Das Element kann nach mehreren versuchen nicht gefunden werden ( die id oder der Name können nicht gefunden werden, trotz aktualisiertem PageSource ),
//		der Test würde auch fehlschlagen da die erste Eingabe nicht übernommen wird ( manuell getestet )
//		
//		WebElement gewaehlteAnmietStationTextField = driver.findElement(By.id("suggester_root"));
//		
//		assertTrue(gewaehlteAnmietStationTextField.getAttribute("value") == "Marburg");
		
	}
	


}
