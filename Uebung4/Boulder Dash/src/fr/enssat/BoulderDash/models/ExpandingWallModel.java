package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.models.DisplayableElementModel;


/**
 * ExpandingWallModel
 *
 * Represents a ExpandingWall in the game.
 *
 * @author      Colin Leverger <me@colinleverger.fr>
 * @since       2015-06-19
 */
public class ExpandingWallModel extends DisplayableElementModel {
	private static String spriteName;
	private static int priority;
	private static String collideSound;
	
	/*
    * Static dataset
    * Specifies the physical parameters of the object
    */
	static {
		spriteName = "expandingwall";
		
		priority = 10;
		
		collideSound = null;
	}
	
    /**
     * Class constructor
     */
	public ExpandingWallModel() {
		super(false, false, spriteName, priority, false, false, false, collideSound);
		this.loadSprite(spriteName);
	}
}