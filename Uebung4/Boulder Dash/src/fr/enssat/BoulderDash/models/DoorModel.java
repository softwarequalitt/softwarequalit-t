package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.models.DisplayableElementModel;


/**
 * DoorModel
 *
 * Represents escape door.
 *
 * @author      Colin Leverger <me@colinleverger.fr>
 * @since       2015-06-19
 */
public class DoorModel extends DisplayableElementModel {
	private static String spriteName;
	private static int priority;
	private static String collideSound;

    /**
     * Static dataset
     * Specifies the physical parameters of the object
     */
	static {
		spriteName = "door";
		priority = 0;
		collideSound = null;
	}

    /**
     * Class constructor
     */
	public DoorModel() {
		super(false, false, spriteName, priority, false, false, false, collideSound);

		this.loadSprite(spriteName);
	}
}
