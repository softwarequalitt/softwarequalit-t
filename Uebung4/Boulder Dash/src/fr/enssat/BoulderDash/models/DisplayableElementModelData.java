package fr.enssat.BoulderDash.models;

public class DisplayableElementModelData {
	public boolean isDestructible;
	public boolean canMove;
	public boolean impactExplosive;
	public boolean animate;
	public boolean falling;

	public DisplayableElementModelData() {
	}
}