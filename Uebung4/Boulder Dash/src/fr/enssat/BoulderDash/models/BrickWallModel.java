package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.models.DisplayableElementModel;


/**
 * BrickWallModel
 *
 * Represents the brick wall in the game.
 *
 * @author      Colin Leverger <me@colinleverger.fr>
 * @since       2015-06-19
 */
public class BrickWallModel extends DisplayableElementModel {
	private static String spriteName;
	private static int priority;
	private static String collideSound;

    /**
     * Static dataset
     * Specifies the physical parameters of the object
     */
	static {
		spriteName = "brickwall";
		priority = 3;
		collideSound = "touch";
	}

    /**
     * Class constructor
     */
	public BrickWallModel() {
		super(true, false, spriteName, priority, false, false, false, collideSound);
        this.loadSprite(spriteName);
	}
}
