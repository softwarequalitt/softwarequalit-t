package fr.enssat.BoulderDash.models;

public class DisplayableElementModelParameter {
	public boolean destructible;
	public boolean moving;
	public String spriteName;
	public int priority;
	public boolean impactExplosive;
	public boolean animate;
	public boolean falling;
	public String collideSound;
	public boolean convertible;

	public DisplayableElementModelParameter(boolean destructible, boolean moving, String spriteName, int priority,
			boolean impactExplosive, boolean animate, boolean falling, String collideSound, boolean convertible) {
		this.destructible = destructible;
		this.moving = moving;
		this.spriteName = spriteName;
		this.priority = priority;
		this.impactExplosive = impactExplosive;
		this.animate = animate;
		this.falling = falling;
		this.collideSound = collideSound;
		this.convertible = convertible;
	}
}