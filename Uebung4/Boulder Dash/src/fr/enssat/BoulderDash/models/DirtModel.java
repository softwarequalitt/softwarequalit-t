package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.models.DisplayableElementModel;


/**
 * DirtModel
 *
 * Represents the dirt in the game.
 *
 * @author      Colin Leverger <me@colinleverger.fr>
 * @since       2015-06-19
 */
public class DirtModel extends DisplayableElementModel {
	private static String spriteName;
	private static int priority;
	private static String collideSound;
	
    /**
     * Static dataset
     * Specifies the physical parameters of the object
     */
	static {
		spriteName = "dirt";
		
		priority = 0;
	
		collideSound = null;
	}

    /**
     * Class constructor
     */
	public DirtModel() {
		super(true, false, spriteName, priority, false, false, false, collideSound);

        this.loadSprite(spriteName);
	}
}