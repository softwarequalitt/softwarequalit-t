package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.models.DisplayableElementModel;


/**
 * SteelWallModel
 *
 * Represents the steelWall
 *
 * @author      Colin Leverger <me@colinleverger.fr>
 * @since       2015-06-19
 */
public class SteelWallModel extends DisplayableElementModel {
	private static String spriteName;
	private static int priority;
	private static String collideSound;

    /**
     * Static dataset
     * Specifies the physical parameters of the object
     */
	static {
		spriteName = "steelwall";
		
		priority = 3;
		
		collideSound = "touch";
	}

    /**
     * Class constructor
     */
	public SteelWallModel() {
		super(false, false, spriteName, priority, false, false, false, collideSound);
		this.loadSprite(spriteName);
	}
}
