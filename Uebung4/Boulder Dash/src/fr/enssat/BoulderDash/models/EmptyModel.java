package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.models.DisplayableElementModel;


/**
 * EmptyModel
 *
 * Represents "nothing".
 *
 * @author      Colin Leverger <me@colinleverger.fr>
 * @since       2015-06-19
 */
public class EmptyModel extends DisplayableElementModel {
	private static String spriteName;
	private static int priority;
	private static String collideSound;

    /**
     * Static dataset
     * Specifies the physical parameters of the object
     */
	static {
		spriteName = "black";
		priority = 0;
		collideSound = null;
	}

    /**
     * Class constructor
     */
	public EmptyModel() {
		super(false, false, spriteName, priority, false, false, false, collideSound);

		this.loadSprite(spriteName);
	}
}
