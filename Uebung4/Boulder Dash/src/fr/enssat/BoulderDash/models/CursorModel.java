package fr.enssat.BoulderDash.models;

import fr.enssat.BoulderDash.models.DisplayableElementModel;


/**
 * CursorModel
 *
 * Represents the field cursor pointer.
 *
 * @author      Valerian Saliou <valerian@valeriansaliou.name>
 * @since       2015-06-22
 */
public class CursorModel extends DisplayableElementModel {
    private static String spriteName;
    private static int priority;
    private static String collideSound;

    /**
     * Static dataset
     * Specifies the physical parameters of the object
     */
    static {
        spriteName = "cursor";
        priority = 0;
        collideSound = null;
    }

    /**
     * Class constructor
     */
    public CursorModel() {
        super(false, false, spriteName, priority, false, false, false, collideSound);

        this.loadSprite(spriteName);
    }
}