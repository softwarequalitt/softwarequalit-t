package fr.enssat.BoulderDash.models;

import java.awt.image.BufferedImage;

public class DisplayableElementModelData {
	public boolean destructible;
	public boolean moving;
	public boolean animate;
	public boolean impactExplosive;
	public String spriteName;
	public int priority;
	public boolean falling;
	public boolean convertible;
	public String collideSound;
	public BufferedImage sprite;

	public DisplayableElementModelData() {
	}
}