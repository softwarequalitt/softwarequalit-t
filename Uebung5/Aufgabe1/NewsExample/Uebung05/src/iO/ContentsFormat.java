package iO;

import model.Article;
import model.Image;
import model.NewsCollection;

public class ContentsFormat implements FormatStrategy {

	NewsCollection rootCollection;
	
	public ContentsFormat(NewsCollection rootCollection) {
		this.rootCollection = rootCollection;
	}

	/**
	 * Print the contents of all directly or transitively referenced items in
	 * the given collection.
	 */

	@Override
	public void print() {
		for (Article article : rootCollection.getArticles()) {
			System.out.println("###" + article.getTitle() + "###");
			System.out.println(article.getContent());
			System.out.println();
		}
		for (Image image : rootCollection.getImages()) {
			System.out.println("###" + image.getTitle() + "###");
			image.drawImage();
			System.out.println();
		}
		for (NewsCollection collection : rootCollection.getCollections()) {
			collection.outputType = FormatType.Contents;
			collection.print();
		}

	}

}
