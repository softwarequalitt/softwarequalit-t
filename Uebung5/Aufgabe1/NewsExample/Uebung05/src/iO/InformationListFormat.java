package iO;

import model.Article;
import model.Image;
import model.NewsCollection;

public class InformationListFormat implements FormatStrategy{

	private NewsCollection rootCollection;
	
	public InformationListFormat(NewsCollection rootCollection) {
		this.rootCollection = rootCollection;

	}

	/**
	 * Print a list showing information about all directly or transitively
	 * referenced items in the given collection.
	 */

	@Override
	public void print() {

		System.out.println("###" + rootCollection.getTopic() + "###");
		for (Article article : rootCollection.getArticles()) {
			System.out.println("Article: " + article.getTitle() + ", Author: " + article.getAuthor());
		}
		for (Image image : rootCollection.getImages()) {
			System.out.println("Image: " + image.getTitle() + ", Resolution: " + image.getWidth() + "x"
					+ image.getHeight() + ", Author: " + image.getAuthor());
		}
		for (NewsCollection collection : rootCollection.getCollections()) {
			collection.outputType = FormatType.InformationList;
			collection.print();
		}

	}

}
