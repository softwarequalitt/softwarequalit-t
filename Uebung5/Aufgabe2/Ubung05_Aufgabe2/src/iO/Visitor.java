package iO;

import model.Article;
import model.Image;
import model.NewsCollection;

public interface Visitor {

	public void visit(Article article);
	public void visit(Image image);
	public void visit(NewsCollection newsCollection);
		
}
