package iO;

import model.Article;
import model.Image;
import model.NewsCollection;
import model.NewsItem;

public class InformationListVisitor implements Visitor {

	@Override
	public void visit(Article article) {
		System.out.println("Article: " + article.getTitle() + ", Author: " 
				+ article.getAuthor());
	}

	@Override
	public void visit(Image image) {
		System.out.println("Image: " + image.getTitle() 
		+ ", Resolution: " + image.getWidth() 
		+ "x" + image.getHeight() 
		+ ", Author: " + image.getAuthor());
		
	}

	@Override
	public void visit(NewsCollection newsCollection) {
		System.out.println("###" + newsCollection.getTopic() + "###");
		for (NewsItem item : newsCollection.getItems()) {

			item.print(this);
			
		}
		
		for (NewsCollection collection : newsCollection.getCollections()) {
			
			collection.print(this);
			
		}
		
		
	}

}
