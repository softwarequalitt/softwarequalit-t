package iO;

import model.Article;
import model.Image;
import model.NewsCollection;
import model.NewsItem;

public class ContentsVisitor implements Visitor{

	@Override
	public void visit(Article article) {
		System.out.println("###" + article.getTitle() + "###");
		System.out.println(article.getContent());
		System.out.println();
		
	}

	@Override
	public void visit(Image image) {
		System.out.println("###" + image.getTitle() + "###");
		image.drawImage();
		System.out.println();
		
	}

	@Override
	public void visit(NewsCollection newsCollection) {
		for(NewsItem item : newsCollection.getItems()){
			
			item.print(this);
			
		}
		for(NewsCollection collection : newsCollection.getCollections()){
			
			collection.print(this);
			
		}
		
	}

}
