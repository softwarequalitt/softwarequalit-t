package test;

import iO.ContentsVisitor;
import iO.InformationListVisitor;
import model.Article;
import model.Image;
import model.NewsCollection;

public class Example {
	
	public static void main(String[] args) {
		NewsCollection masterCollection = new NewsCollection("Sports");
		masterCollection.addCollection(new NewsCollection("Tennis")
				.addItems(new Article("Aaron Carter wins Wimbledon","ups"))
				.addItems(new Image("The Becker-Faust", 800, 600, "zdf"))
				.addItems(new Image("Aaron Carter smiling", 1280, 720, "anonymous"))
				.addItems(new Article("Greenpeace riots at Wimbledon finals: "
						+ "the grass is always greener on the artificial side", "dpd")))
		.addCollection(new NewsCollection("Formula 1")
				.addItems(new Article("Mika H�kkinen back on track", "pdf"))
				.addItems(new Article("Alonso driving wrong-way at the Monaco Grand Prix", "ard")))
		.addCollection(new NewsCollection("Soccer")
				.addCollection(new NewsCollection("German Soccer")
						.addItems(new Image("Rooney parody", 800, 600, "anonymous"))
						.addItems(new Article("Paul Simon prays for Luis Suarez's soul", "gps"))
				.addCollection(new NewsCollection("UK Soccer")		
						.addItems(new Article("Oliver Kahn signs trainer contract at Schalke 04", "ddr")))));
		
	
		masterCollection.print(new InformationListVisitor());
		
		System.out.println("\n=================\n");
		
		masterCollection.print(new ContentsVisitor());
	}
}
