package model;

import iO.Visitor;

public abstract class NewsComponent {

	protected String title;

	public abstract void print(Visitor visitor);
	
}
